import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { Http,HttpModule } from '@angular/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';
import { DatepickerModule, AlertModule, ModalModule } from 'ng2-bootstrap';
import { AppComponent }   from './app.component';
import { HeaderComponent } from './pages/header/header.component';
import { FooterComponent } from './pages/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { WindowRef } from './services/windowref.service';
import { NewService } from './services/new.service';


@NgModule({
  imports:      [
      BrowserModule,
      FormsModule,
      Angular2FontawesomeModule,
      RouterModule.forRoot([
          {path: '', component: HomeComponent},
          {path: 'home', component: HomeComponent},
          {path: 'about', component: AboutComponent},
          // {path: 'about', component: HomeComponent},
          // {path: 'home', component: HomeComponent},
      ]),
      AlertModule.forRoot(),
      DatepickerModule.forRoot(),
      ModalModule.forRoot(),
      HttpModule,
      ReactiveFormsModule,
  ],
  declarations: [
      AppComponent,
      HeaderComponent,
      FooterComponent,
      HomeComponent,
      AboutComponent,
  ],
  providers: [
      NewService,
      WindowRef,
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
