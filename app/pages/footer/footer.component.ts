import { Component } from '@angular/core';

@Component({
    selector: 'footer-app',
    templateUrl: 'app/pages/footer/footer.component.html',
    styleUrls: ['app/assets/css/style.css'],
})

export class FooterComponent{

    public logo_url:string = 'app/assets/img/logo_header.png';

}