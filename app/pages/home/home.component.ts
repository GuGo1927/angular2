import {
    Component ,
    trigger,
    state,
    style,
    transition,
    animate,
} from '@angular/core';
import { WindowRef } from '../../services/windowref.service';
import {HeaderComponent} from "../header/header.component";
import {NewService} from "../../services/new.service";

@Component({
    selector: 'home-app',
    templateUrl: 'app/pages/home/home.component.html',
    styleUrls: ['app/assets/css/style.css'],
    animations: [
        trigger('home_desc', [
            state('off', style({
                display:'none',
            })),
            state('on',style({
                display:'block',
            })),
        ]),
        trigger('home_name', [
            state('off', style({
                display:'none',
            })),
            state('on',style({
                display:'block',
            })),
        ]),
    ],
    providers:[HeaderComponent]
})

export class HomeComponent{
    public home_logo:string = 'app/assets/img/logo-big.png';
    public window_height:number;
    public description_user: string = 'I am Full Stack Web developer'
    public desc_home:string = 'off';
    public home_name_show:string = 'off';
    public home_name:string = 'Gurgen Yeghiazaryan';

    constructor(private _newService: NewService,private winref: WindowRef,private headerCom: HeaderComponent){
        this.window_height = winref.nativeWindow.innerHeight - 81;
        if(this._newService.isAuth == true){
            this.headerCom.headerIsAuth = false;
        }
        else{
            this.headerCom.headerIsAuth = false;
        }
    }

    /**
     * Home Description change
     * @param event
     */
    public change_desc_user(event){
        this.description_user = event.target.value;
        this.desc_home = 'off';
    }
    public show_home_desc_box(){
        this.desc_home = (this.desc_home == 'off') ? 'on' : 'off';
    }

    /**
     *  Home User Name Change
     */
    public show_home_name_box(){
        this.home_name_show = (this.home_name_show == 'off') ? 'on' : 'off';
    }
    public change_home_name(event){
        console.log(event.target.value);
        this.home_name = event.target.value;
        this.home_name_show = 'off';
    }

}