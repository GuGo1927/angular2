"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var windowref_service_1 = require("../../services/windowref.service");
var header_component_1 = require("../header/header.component");
var new_service_1 = require("../../services/new.service");
var HomeComponent = (function () {
    function HomeComponent(_newService, winref, headerCom) {
        this._newService = _newService;
        this.winref = winref;
        this.headerCom = headerCom;
        this.home_logo = 'app/assets/img/logo-big.png';
        this.description_user = 'I am Full Stack Web developer';
        this.desc_home = 'off';
        this.home_name_show = 'off';
        this.home_name = 'Gurgen Yeghiazaryan';
        this.window_height = winref.nativeWindow.innerHeight - 81;
        if (this._newService.isAuth == true) {
            this.headerCom.headerIsAuth = false;
        }
        else {
            this.headerCom.headerIsAuth = false;
        }
    }
    /**
     * Home Description change
     * @param event
     */
    HomeComponent.prototype.change_desc_user = function (event) {
        this.description_user = event.target.value;
        this.desc_home = 'off';
    };
    HomeComponent.prototype.show_home_desc_box = function () {
        this.desc_home = (this.desc_home == 'off') ? 'on' : 'off';
    };
    /**
     *  Home User Name Change
     */
    HomeComponent.prototype.show_home_name_box = function () {
        this.home_name_show = (this.home_name_show == 'off') ? 'on' : 'off';
    };
    HomeComponent.prototype.change_home_name = function (event) {
        console.log(event.target.value);
        this.home_name = event.target.value;
        this.home_name_show = 'off';
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    core_1.Component({
        selector: 'home-app',
        templateUrl: 'app/pages/home/home.component.html',
        styleUrls: ['app/assets/css/style.css'],
        animations: [
            core_1.trigger('home_desc', [
                core_1.state('off', core_1.style({
                    display: 'none',
                })),
                core_1.state('on', core_1.style({
                    display: 'block',
                })),
            ]),
            core_1.trigger('home_name', [
                core_1.state('off', core_1.style({
                    display: 'none',
                })),
                core_1.state('on', core_1.style({
                    display: 'block',
                })),
            ]),
        ],
        providers: [header_component_1.HeaderComponent]
    }),
    __metadata("design:paramtypes", [new_service_1.NewService, windowref_service_1.WindowRef, header_component_1.HeaderComponent])
], HomeComponent);
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map