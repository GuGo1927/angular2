import { Component, OnInit } from '@angular/core';
import { Http,Response,Headers } from "@angular/http";
import { NewService } from '../../services/new.service';

@Component({
    selector: 'about-app',
    templateUrl: 'app/pages/about/about.component.html',
    styleUrls: ['app/assets/css/style.css'],
})

export class AboutComponent implements OnInit{
    public about_img_url:string = 'app/assets/img/about.jpg';
    public upload_img_src:string;
    public url:string;
    public aboutSer : any;
    private errorMsg: string;
    constructor(private _newservice: NewService){

    }
    ngOnInit(){
        this._newservice.getProduct()
            .subscribe(
                (res) => {
                    this.aboutSer = res;

                },
                error => this.errorMsg = <any>error
            );
    }
}