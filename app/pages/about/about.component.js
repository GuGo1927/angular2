"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var new_service_1 = require("../../services/new.service");
var AboutComponent = (function () {
    function AboutComponent(_newservice) {
        this._newservice = _newservice;
        this.about_img_url = 'app/assets/img/about.jpg';
    }
    AboutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._newservice.getProduct()
            .subscribe(function (res) {
            _this.aboutSer = res;
        }, function (error) { return _this.errorMsg = error; });
    };
    return AboutComponent;
}());
AboutComponent = __decorate([
    core_1.Component({
        selector: 'about-app',
        templateUrl: 'app/pages/about/about.component.html',
        styleUrls: ['app/assets/css/style.css'],
    }),
    __metadata("design:paramtypes", [new_service_1.NewService])
], AboutComponent);
exports.AboutComponent = AboutComponent;
//# sourceMappingURL=about.component.js.map