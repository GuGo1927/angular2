"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var new_service_1 = require("../../services/new.service");
var HeaderComponent = (function () {
    function HeaderComponent(_newService, router) {
        this._newService = _newService;
        this.router = router;
        this.logo_url = 'app/assets/img/logo_header.png';
        this.headerIsAuth = false;
        this.wrongLogin = false;
        this.loginForm = new forms_1.FormGroup({
            user_mail: new forms_1.FormControl(null, forms_1.Validators.required),
            user_pass: new forms_1.FormControl(null, forms_1.Validators.required)
        });
        if (sessionStorage.getItem('user')) {
            this.headerIsAuth = this._newService.isAuth = true;
            this.userData = sessionStorage.getItem('user');
        }
        else {
            console.log(222);
        }
        console.log(222);
    }
    HeaderComponent.prototype.doLogin = function () {
        var _this = this;
        var data = this.loginForm.value;
        this._newService.checkLogin(this.loginForm.value)
            .subscribe(function (res) {
            _this.loginService = res;
            if (_this.loginService.success == 1) {
                _this.headerIsAuth = true;
                sessionStorage.setItem('user', JSON.parse(_this.loginService.data));
                _this.router.navigate(['/home']);
            }
            else {
                _this.headerIsAuth = false;
                _this.wrongLogin = true;
                setTimeout(function () {
                    _this.wrongLogin = false;
                }, 4000);
            }
        }, function (error) { return _this.errorLogin = error; });
    };
    HeaderComponent.prototype.logOut = function () {
        sessionStorage.removeItem('user');
        this.headerIsAuth = this._newService.isAuth = false;
    };
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    core_1.Component({
        selector: 'header-app',
        templateUrl: 'app/pages/header/header.component.html',
        styleUrls: ['app/assets/css/style.css', 'node_modules/font-awesome/css/font-awesome.css'],
    }),
    __metadata("design:paramtypes", [new_service_1.NewService, router_1.Router])
], HeaderComponent);
exports.HeaderComponent = HeaderComponent;
//# sourceMappingURL=header.component.js.map