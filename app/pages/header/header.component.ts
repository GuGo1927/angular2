import { Component, Input, } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import {NewService} from "../../services/new.service";

@Component({
    selector: 'header-app',
    templateUrl: 'app/pages/header/header.component.html',
    styleUrls: ['app/assets/css/style.css','node_modules/font-awesome/css/font-awesome.css'],
})

export class HeaderComponent{
    public logo_url:string = 'app/assets/img/logo_header.png';
    public userData:any;
    public headerIsAuth:boolean = false;
    public loginService:any;
    public errorLogin:any;
    public wrongLogin:boolean = false;
    public loginModalBox:string;

    loginForm = new FormGroup({
        user_mail: new FormControl(null, Validators.required),
        user_pass: new FormControl(null, Validators.required)
    });



    constructor(private _newService: NewService,private router:Router,){
        if(sessionStorage.getItem('user')){
            this.headerIsAuth = this._newService.isAuth = true;
            this.userData = sessionStorage.getItem('user');
        }
        else{
            console.log(222)
        }
        console.log(222)
    }
    public doLogin() {
        let data = this.loginForm.value;
        this._newService.checkLogin(this.loginForm.value)
            .subscribe(
                (res) => {
                    this.loginService = res;
                    if(this.loginService.success == 1){
                        this.headerIsAuth = true;
                        sessionStorage.setItem('user', JSON.parse(this.loginService.data));
                        this.router.navigate(['/home']);
                    }
                    else{
                        this.headerIsAuth = false;
                        this.wrongLogin = true;
                        setTimeout(() => {
                            this.wrongLogin = false;
                        }, 4000);
                    }
                },
                error => this.errorLogin = <any>error
            );
    }





    public logOut(){
        sessionStorage.removeItem('user');
        this.headerIsAuth = this._newService.isAuth = false;
    }


}