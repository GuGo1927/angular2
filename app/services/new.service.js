"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/throw");
var NewService = (function () {
    function NewService(_http) {
        this._http = _http;
        this.urlSend = 'http://dev.ang_back/ApiController/get_all_users/';
        this.urlLogin = 'http://dev.ang_back/ApiController/checkLogin/';
        this.isAuth = false;
    }
    NewService.prototype.getProduct = function () {
        return this._http.get(this.urlSend)
            .map(function (res) { return res.json(); });
    };
    NewService.prototype.checkLogin = function (data) {
        var headers = new http_1.Headers;
        var params = "username=" + data.user_mail + "&password=" + data.user_pass;
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this._http.post(this.urlLogin, params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    NewService.prototype.handleError = function (error) {
        return Observable_1.Observable.throw(error.json().error || "Server Error");
    };
    return NewService;
}());
NewService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], NewService);
exports.NewService = NewService;
//# sourceMappingURL=new.service.js.map