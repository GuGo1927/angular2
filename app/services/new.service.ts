import { Injectable } from '@angular/core';
import { Http,Response,Headers } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import any = jasmine.any;

@Injectable()
export class NewService{
    private urlSend:string = 'http://dev.ang_back/ApiController/get_all_users/';
    private urlLogin:string = 'http://dev.ang_back/ApiController/checkLogin/';
    public isAuth:boolean = false;
    constructor(private _http:Http){

    }
    getProduct(): Observable<any>{

        return this._http.get(this.urlSend)
            .map(res => res.json());
    }

    checkLogin(data): Observable<any>{
        var headers = new Headers;
        var params = "username="+data.user_mail+"&password="+data.user_pass;
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this._http.post(this.urlLogin, params, {headers: headers})
            .map(res => res.json());
    }


    private handleError (error: Response) {
        return Observable.throw(error.json().error || "Server Error");
    }


}