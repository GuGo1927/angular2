import { Component } from '@angular/core';
import { NewService } from './services/new.service';

@Component({
  selector: 'my-app',
  template: `<header-app></header-app>
                <div class="content_site">
                    
                </div>    
                <router-outlet></router-outlet>
              <footer-app></footer-app>
              `,
  styleUrls: ['app/assets/css/style.css'],
  providers: [ NewService ],
})
export class AppComponent {


}
