"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require("@angular/router");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var angular2_fontawesome_1 = require("angular2-fontawesome/angular2-fontawesome");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var app_component_1 = require("./app.component");
var header_component_1 = require("./pages/header/header.component");
var footer_component_1 = require("./pages/footer/footer.component");
var home_component_1 = require("./pages/home/home.component");
var about_component_1 = require("./pages/about/about.component");
var windowref_service_1 = require("./services/windowref.service");
var new_service_1 = require("./services/new.service");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            angular2_fontawesome_1.Angular2FontawesomeModule,
            router_1.RouterModule.forRoot([
                { path: '', component: home_component_1.HomeComponent },
                { path: 'home', component: home_component_1.HomeComponent },
                { path: 'about', component: about_component_1.AboutComponent },
            ]),
            ng2_bootstrap_1.AlertModule.forRoot(),
            ng2_bootstrap_1.DatepickerModule.forRoot(),
            ng2_bootstrap_1.ModalModule.forRoot(),
            http_1.HttpModule,
            forms_1.ReactiveFormsModule,
        ],
        declarations: [
            app_component_1.AppComponent,
            header_component_1.HeaderComponent,
            footer_component_1.FooterComponent,
            home_component_1.HomeComponent,
            about_component_1.AboutComponent,
        ],
        providers: [
            new_service_1.NewService,
            windowref_service_1.WindowRef,
        ],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map